ARG ARCH="amd64"
ARG OS="linux"
FROM quay.io/prometheus/busybox-${OS}-${ARCH}:latest

COPY pubsubbeat     /bin/pubsubbeat
COPY fields.yml     /etc/pubsubbeat/fields.yml
COPY pubsubbeat.yml /etc/pubsubbeat/pubsubbeat.yml
RUN chmod 644 /etc/pubsubbeat/pubsubbeat.yml

EXPOSE     5066
USER       nobody
WORKDIR    /etc/pubsubbeat
ENTRYPOINT ["/bin/pubsubbeat"]
