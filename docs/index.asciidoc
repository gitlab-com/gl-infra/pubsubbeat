= Pubsubbeat Docs

Welcome to the Pubsubbeat documentation.

== Compressed batches

Pubsubbeat offers support for compressed batches via the following message
attributes:

* `pubsubbeat.compression`: Only one value is supported at present, `gzip`. If
  this value is set, pubsubbeat will decompress the value. For JSON batches,
  this can reduce message size in pubsub quite a bit.

* `pubsubbeat.batch_ndjson`: Setting the value of this attribute to `true` will
  make pubsubbeat treat the message body as a newline-delimited batch. This
  allows multiple events to be batched (and compressed) together, making them
  compression more effective.

Toggling the behaviour is done by the sender. This allows for incremental
rollouts that are forwards- and backwards-compatible.

Here is a sample implementation in ruby (e.g. for use with fluentd):

```
batched = msgs.join("\n")
compressed = Zlib.gzip(batched, level: @gzip_level)
@client.publish(
  compressed,
  'pubsubbeat.batch_ndjson' => 'true',
  'pubsubbeat.compression' => 'gzip'
)
```
